#-*- coding: utf-8 -*-
import os
from telegram.ext import Updater
from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram.ext import MessageHandler, Filters
from telegram import InlineKeyboardButton, InlineKeyboardMarkup


#Variables globales 
total = []
mensajes = []
listaParticipantes = [[]]
chat_id = []
mensaje_quedada = []

keyboard = [[InlineKeyboardButton("Yo voy!", callback_data="voy"),
				InlineKeyboardButton("No voy :(", callback_data="no_voy")]]
replyMarkup = InlineKeyboardMarkup(keyboard)

def start(bot, update):
	""" This function will be executed when '/start' command is received """
	mensaje_start = ("De acuerdo, ya estoy listo para registrar vuestras quedadas!!")
	bot.send_message(chat_id=update.message.chat_id, text=mensaje_start)


def clear(bot, update):
	""" This function will be executed when '/clear' command is received """
	id = update.message.chat_id
	if id not in chat_id:
		mensaje = "Primero utiliza el comando '/quedada'"
		update.message.reply_text(str(mensaje), parse_mode="markdown")
	else:
		indice = chat_id.index(id)
		global total
		total[indice] = 0
		global listaParticipantes
		listaParticipantes[indice] = []
		global mensaje_quedada
		mensaje_quedada[indice] = ""
		global mensajes
		mensajes[indice] = "De momento van " + str(total[indice]) + " personas." 
		mensaje = "Ahora utiliza '/quedada' junto al texto de la quedada"
		update.message.reply_text(str(mensaje), parse_mode="markdown")


def quedada(bot, update):
	""" This function will be executed when '/quedada' command is received """

	texto = update.message.text
	id = update.message.chat_id
	if len(texto) == 8 or len(texto) == 26:	#si recibe '\quedada' (por privado o por grupo)
		texto = ""
	else: #el campo texto no esta vacio
		texto = texto.split(None, 1)[1]
	if not texto:	#entra si texto es vacio 
		texto = ""
	else:
		texto = texto + '\n'
	global chat_id
	global mensajes
	global total
	global listaParticipantes 
	global mensaje_quedada
	if id not in chat_id: #solo entra en el if si es la primera vez que se usa el bot
		chat_id.append(id)	#Añade el id del chat a la lista de chats
		indice = chat_id.index(id)
		mensajes.append("De momento van 0 personas")
		total.append(0)
		listaParticipantes.append([])
		mensaje_quedada.append([])

	else: #Ya se encuentra en la lista, limpia todo y escribe nuevo mensaje
		indice = chat_id.index(id)
		total[indice] = 0
		listaParticipantes[indice] = []
		mensajes[indice] = "De momento van " + str(total[indice]) + " personas." 

	indice = chat_id.index(id)
	mensaje_quedada[indice] = texto
	if not mensaje_quedada[indice]: #si el texto es vacio
		mensaje = mensajes[indice]
	else:
		mensaje = mensaje_quedada[indice] + mensajes[indice]	
	update.message.reply_text(str(mensaje), parse_mode="markdown", reply_markup=replyMarkup)

def anyadir_usuario(usuario, indice):
	#añade un nombre de usuario a la listaParticipantes
	global listaParticipantes
	listaParticipantes[indice].append(usuario)
	aux = listaParticipantes[indice]
	global total
	total[indice] += 1

	global mensajes
	mensajes[indice] = "De momento van " + str(total[indice]) + " personas:\n"
	for p in listaParticipantes[indice]:
		mensajes[indice] = (mensajes[indice] + p + "\n")

def quitar_usuario(usuario, indice):
	global listaParticipantes
	listaParticipantes[indice].remove(usuario)
	global total
	total[indice] -= 1
	global mensaje
	mensajes[indice] = "De momento van " + str(total[indice]) + " personas:\n"
	for p in listaParticipantes[indice]:
		mensaje = (mensaje + p + "\n")

def button(bot, update):
	query = update.callback_query
	chat = query.message.chat_id
	#Elimina el relojito de espera cuando pulsas un boton 
	bot.answerCallbackQuery(query.id)
	usuario = query.from_user.first_name
	global chat_id
	indice = chat_id.index(chat)
	global listaParticipantes
	if query.data == "voy" and usuario not in listaParticipantes[indice]:
		anyadir_usuario(usuario,indice)
	elif query.data == "no_voy" and usuario in listaParticipantes[indice]:
		quitar_usuario(usuario,indice)
	else:
		pass
	
	

	mensaje = mensaje_quedada[indice] + mensajes[indice]
	bot.edit_message_text(text=str(mensaje),
		chat_id=query.message.chat_id,
		message_id=query.message.message_id,
		parse_mode="markdown",
		reply_markup=replyMarkup)


def refloat(bot, update):
	""" This function will be executed when '/refloat' command is received """
	id = update.message.chat_id
	if id not in chat_id:
		mensaje = "Primero utiliza el comando '/quedada'"
		update.message.reply_text(str(mensaje), parse_mode="markdown")

	indice = chat_id.index(id)
	mensaje = mensaje_quedada[indice] + mensajes[indice]
	update.message.reply_text(str(mensaje), parse_mode="markdown", reply_markup=replyMarkup)



def main(bot_token):
	""" Main function of the bot """
	updater = Updater(token=bot_token)
	dispatcher = updater.dispatcher


	# Command handlers
	start_handler = CommandHandler('start', start)
	quedada_handler = CommandHandler('quedada', quedada)
	clear_handler = CommandHandler('clear', clear)
	refloat_handler = CommandHandler('refloat', refloat)
	updater.dispatcher.add_handler(CallbackQueryHandler(button))

	# Add the handlers to the bot
	dispatcher.add_handler(start_handler)
	dispatcher.add_handler(quedada_handler)
	dispatcher.add_handler(clear_handler)
	dispatcher.add_handler(refloat_handler)

	# Starting the bot
	updater.start_polling(allowed_updates=[])

if __name__ == "__main__":
	TOKEN = os.environ["token_bot_quedadas"]
	main(TOKEN)
