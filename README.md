# Quedadas - Telegram Bot
Bot de telegram para organizar reuniones en grupos con un gran número de integrantes que no requiere registro previo por parte de estos.

# Pruebas
El bot está actualmente operativo en [Telegram](https://t.me/SuperQuedadas_bot) para poder comprobar el funcionamiento del mismo con una sola persona en privado

# Uso
El bot dispone de los comandos:
- '/start': Inicia el servicio del bot
- '/quedada': crea una nueva reunión en el que mostrará el número de personas totales que quieren asistir, así como la lista con los nombres de estas. <br/>
  el comando puede estar seguido de texto (y emoticonos) que se mostarán antes del mensaje con el número de participantes totales.
- '/refloat': vuelve a enviar la lista de asistentes para que el mensaje pueda volver a ser visible si se pierde debido a otras conversaciones en el grupo.
- '/clear': Elimina la reunión permitiendo crear una nueva. 
  - **Nota:** actualmente no es necesario utilizar esta función ya que puede usarse de nuevo '/quedada' y automáticamente eliminará la reunión anterior y creará una nueva

# License
Este bot utiliza una licencia GPLv3 - Ver [LICENSE](https://gitlab.com/pulsar-unizar/super-quedadas-bot/blob/master/LICENSE) para más detalles